#!/bin/bash
#
# backup.sh
# 
# (c) Niki Kovacs 2020 <info@microlinux.fr>
# 
# Daily backups using Rsnapshot.
#
# 1. Move the script to a sensible place like ~/bin.
#
# 2. Adapt the variables to your system.
#
# 3. Define a cronjob like this:
#
#    30 09 * * * /home/microlinux/bin/backup.sh

#HOSTNAME=$(hostname --fqdn)
#HOSTNAME=backup.microlinux.fr
SENDER="root@$HOSTNAME"
#RELAY="yatahongaga@gmail.com" <== LAN
#RELAY="$SENDER"               <== Dedibox
ADMIN="info@microlinux.fr"

rsnapshot -v daily 2>&1 | \
  mail -s "Daily backup report for $HOSTNAME" \
  -r "$RELAY (root@$HOSTNAME)" \
  $ADMIN
